/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlideShow показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlideShow -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

  // var ourSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  
// 
  settings = {
    ourSliderImages: ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg'],
    sliderId: 'slider',
    animationStyle: ['slide-transform', 'showing-transform']
  }
  
  settings2 = {
    ourSliderImages: ['images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'],
    sliderId: 'slider1',
    animationStyle: ['slide', 'showing']
  }
  
  
  function Slider(settings) {
    var currentPosition = 0;
    var currentSlide;
    var sliderDiv = document.getElementById(settings.sliderId);
    sliderDiv.className = 'slider';
    var nextSlideShowButt = document.createElement('button');
    nextSlideShowButt.innerText = 'Next';
    var prevSlideButt = document.createElement('button');
    prevSlideButt.innerText = 'Prev';
    var headerSliderControls = document.createElement('header');
    sliderDiv.appendChild(headerSliderControls);
    headerSliderControls.appendChild(prevSlideButt);
    headerSliderControls.appendChild(nextSlideShowButt);
  
    // Функция появления слайда
    function slideAppear(elem) {
      if (currentSlide != undefined) {
        sliderDiv.removeChild(currentSlide);
        currentSlide = document.createElement('img');
        currentSlide.setAttribute('src', this.ourSliderImages[elem]);
      } else {
        currentSlide = document.createElement('img');
        currentSlide.setAttribute('src', this.ourSliderImages[elem]);
      }
    }
  
    slideAppear.call(settings, currentPosition);
    sliderDiv.appendChild(currentSlide);

    var onClickButtNext = function(event) {
      // debugger;
      if (currentPosition == settings.ourSliderImages.length - 1) { currentPosition = 0}
      else { currentPosition++; }
      slideAppear.call(settings, currentPosition);
      currentSlide.className = settings.animationStyle[0];
      currentSlide.classList.remove(settings.animationStyle[2]);
      sliderDiv.appendChild(currentSlide);
      function nextSlideShowR() {
        currentSlide.classList.add(settings.animationStyle[1]);
      }
      nextSlideShowRR = nextSlideShowR.bind(this);
      var slideInterval = setInterval(nextSlideShowRR, 50);
    }

    nextSlideShowButt.addEventListener('click', onClickButtNext.bind(this), false);

    var onClickButtPrev = function(event) {
      if (currentPosition == 0) { currentPosition = settings.ourSliderImages.length - 1}
      else { currentPosition--; }
      slideAppear.call(settings, currentPosition);
      currentSlide.className = settings.animationStyle[0];
      currentSlide.classList.remove(settings.animationStyle[1]);
      sliderDiv.appendChild(currentSlide);
      function nextSlideShowL() {
        currentSlide.classList.add(settings.animationStyle[1]);
      }
      nextSlideShowLL = nextSlideShowL.bind(this);
      var slideInterval = setInterval(nextSlideShowLL, 50);
    }

    prevSlideButt.addEventListener('click', onClickButtPrev.bind(this));
  }

  Slider(settings);
  Slider(settings2);
